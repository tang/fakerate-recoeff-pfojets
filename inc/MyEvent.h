//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Dec 13 20:08:21 2011 by ROOT version 5.28/00e
// from TTree susy/susy
// found on file: NTUP_SUSY.606377._000248.root.1
//////////////////////////////////////////////////////////

#ifndef MyEvent_h
#define MyEvent_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TLorentzVector.h>
#include <vector>
#include <string>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TCanvas.h>
#include <TRandom2.h>
#include <TRandom3.h>

class MyEvent : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain

   // output files
   TFile *ofile;

   int evtcount;
   float fraclast;
   int evtmax;

   //Declare histograms

   //TH1D *DeltaR_allPFO_allPFOTS_hist;

   //TH1D *h_allPFO_PtChargedFraction_hist;
   //TH1D *h_allPFOTS_PtChargedFraction_hist;


   TProfile     *h_allPFO_AveJetsNum_NPV_hist;
   //TProfile     *h_allPFO_AveJetsNum_NPV_pt20to25_hist;   
   //TProfile     *h_allPFO_AveJetsNum_NPV_pt25to30_hist;   
   TProfile     *h_allPFOTS_AveJetsNum_NPV_hist;
   //TProfile     *h_allPFOTS_AveJetsNum_NPV_pt20to25_hist;   
   //TProfile     *h_allPFOTS_AveJetsNum_NPV_pt25to30_hist;   
   TProfile     *h_allPFO_AveJetsNum_Mu_hist;
   //TProfile     *h_allPFO_AveJetsNum_Mu_pt20to25_hist;   
   //TProfile     *h_allPFO_AveJetsNum_Mu_pt25to30_hist;   
   TProfile     *h_allPFOTS_AveJetsNum_Mu_hist;
   //TProfile     *h_allPFOTS_AveJetsNum_Mu_pt20to25_hist;   
   //TProfile     *h_allPFOTS_AveJetsNum_Mu_pt25to30_hist;   
   TProfile     *h_stdjet_AveJetsNum_NPV_hist;
   TProfile     *h_stdjet_AveJetsNum_Mu_hist;

   TH1D     *h_allPFO_TruthJets_DeltaR_hist;  
   TH1D     *h_allPFOTS_TruthJets_DeltaR_hist;  
   TH1D     *h_stdjet_TruthJets_DeltaR_hist;  

   TH1D     *h_allPFO_PtResponse_hist;
   TH1D     *h_allPFO_PtResponse_NPV00to20_hist;
   TH1D     *h_allPFO_PtResponse_NPV20to40_hist;
   TH1D     *h_allPFO_PtResponse_NPV40to50_hist;
   TH1D     *h_allPFOTS_PtResponse_hist;
   TH1D     *h_allPFOTS_PtResponse_NPV00to20_hist;
   TH1D     *h_allPFOTS_PtResponse_NPV20to40_hist;
   TH1D     *h_allPFOTS_PtResponse_NPV40to50_hist;
   TH1D     *h_stdjet_PtResponse_hist;
   TH1D     *h_stdjet_PtResponse_NPV00to20_hist;
   TH1D     *h_stdjet_PtResponse_NPV20to40_hist;
   TH1D     *h_stdjet_PtResponse_NPV40to50_hist;

   TH1D     *h_allPFO_FakeJetsNum_NPV_pt20to25_hist;
   TH1D     *h_allPFO_FakeJetsNum_NPV_pt25to30_hist;
   TH1D     *h_allPFO_JetsNum_NPV_pt20to25_hist;
   TH1D     *h_allPFO_JetsNum_NPV_pt25to30_hist;
   TH1D     *h_allPFOTS_FakeJetsNum_NPV_pt20to25_hist;
   TH1D     *h_allPFOTS_FakeJetsNum_NPV_pt25to30_hist;
   TH1D     *h_allPFOTS_JetsNum_NPV_pt20to25_hist;
   TH1D     *h_allPFOTS_JetsNum_NPV_pt25to30_hist;

   TH1D     *h_allPFO_FakeRate_Pt_hist;
   TH1D     *h_allPFO_FakeRate_Eta_hist;
   TProfile *h_allPFO_FakeRate_Mu_hist;
   TProfile *h_allPFO_FakeRate_Mu_pt10to15_hist;
   TProfile *h_allPFO_FakeRate_Mu_pt15to20_hist;
   TProfile *h_allPFO_FakeRate_Mu_pt20to25_hist;
   TProfile *h_allPFO_FakeRate_Mu_pt25to30_hist;
   TProfile *h_allPFO_FakeRate_Mu_pt30to40_hist;
   TProfile *h_allPFO_FakeRate_Mu_pt40to60_hist;
   TProfile *h_allPFO_FakeRate_Mu_pt60to120_hist;
   TProfile *h_allPFO_FakeRate_Mu_pt120to1000_hist;
   TProfile *h_allPFO_FakeRate_NPV_hist;
   TProfile *h_allPFO_FakeRate_NPV_pt10to15_hist;
   TProfile *h_allPFO_FakeRate_NPV_pt15to20_hist;
   TProfile *h_allPFO_FakeRate_NPV_pt20to25_hist;
   TProfile *h_allPFO_FakeRate_NPV_pt25to30_hist;
   TProfile *h_allPFO_FakeRate_NPV_pt30to40_hist;
   TProfile *h_allPFO_FakeRate_NPV_pt40to60_hist;
   TProfile *h_allPFO_FakeRate_NPV_pt60to120_hist;
   TProfile *h_allPFO_FakeRate_NPV_pt120to1000_hist;
   TH1D     *h_allPFOTS_FakeRate_Pt_hist;
   TH1D     *h_allPFOTS_FakeRate_Eta_hist;
   TProfile *h_allPFOTS_FakeRate_Mu_hist;
   TProfile *h_allPFOTS_FakeRate_Mu_pt10to15_hist;
   TProfile *h_allPFOTS_FakeRate_Mu_pt15to20_hist;
   TProfile *h_allPFOTS_FakeRate_Mu_pt20to25_hist;
   TProfile *h_allPFOTS_FakeRate_Mu_pt25to30_hist;
   TProfile *h_allPFOTS_FakeRate_Mu_pt30to40_hist;
   TProfile *h_allPFOTS_FakeRate_Mu_pt40to60_hist;
   TProfile *h_allPFOTS_FakeRate_Mu_pt60to120_hist;
   TProfile *h_allPFOTS_FakeRate_Mu_pt120to1000_hist;
   TProfile *h_allPFOTS_FakeRate_NPV_hist;
   TProfile *h_allPFOTS_FakeRate_NPV_pt10to15_hist;
   TProfile *h_allPFOTS_FakeRate_NPV_pt15to20_hist;
   TProfile *h_allPFOTS_FakeRate_NPV_pt20to25_hist;
   TProfile *h_allPFOTS_FakeRate_NPV_pt25to30_hist;
   TProfile *h_allPFOTS_FakeRate_NPV_pt30to40_hist;
   TProfile *h_allPFOTS_FakeRate_NPV_pt40to60_hist;
   TProfile *h_allPFOTS_FakeRate_NPV_pt60to120_hist;
   TProfile *h_allPFOTS_FakeRate_NPV_pt120to1000_hist;
   TH1D     *h_stdjet_FakeRate_Pt_hist;
   TH1D     *h_stdjet_FakeRate_Eta_hist;
   TProfile *h_stdjet_FakeRate_Mu_hist;
   TProfile *h_stdjet_FakeRate_Mu_pt10to15_hist;
   TProfile *h_stdjet_FakeRate_Mu_pt15to20_hist;
   TProfile *h_stdjet_FakeRate_Mu_pt20to25_hist;
   TProfile *h_stdjet_FakeRate_Mu_pt25to30_hist;
   TProfile *h_stdjet_FakeRate_Mu_pt30to40_hist;
   TProfile *h_stdjet_FakeRate_Mu_pt40to60_hist;
   TProfile *h_stdjet_FakeRate_Mu_pt60to120_hist;
   TProfile *h_stdjet_FakeRate_Mu_pt120to1000_hist;
   TProfile *h_stdjet_FakeRate_NPV_hist;
   TProfile *h_stdjet_FakeRate_NPV_pt10to15_hist;
   TProfile *h_stdjet_FakeRate_NPV_pt15to20_hist;
   TProfile *h_stdjet_FakeRate_NPV_pt20to25_hist;
   TProfile *h_stdjet_FakeRate_NPV_pt25to30_hist;
   TProfile *h_stdjet_FakeRate_NPV_pt30to40_hist;
   TProfile *h_stdjet_FakeRate_NPV_pt40to60_hist;
   TProfile *h_stdjet_FakeRate_NPV_pt60to120_hist;
   TProfile *h_stdjet_FakeRate_NPV_pt120to1000_hist;

   TProfile *h_allPFO_RecoEff_Pt_hist;
   TProfile *h_allPFO_RecoEff_Eta_hist;
   TProfile *h_allPFO_RecoEff_Mu_hist;
   TProfile *h_allPFO_RecoEff_Mu_pt10to15_hist;
   TProfile *h_allPFO_RecoEff_Mu_pt15to20_hist;
   TProfile *h_allPFO_RecoEff_Mu_pt20to25_hist;
   TProfile *h_allPFO_RecoEff_Mu_pt25to30_hist;
   TProfile *h_allPFO_RecoEff_Mu_pt30to40_hist;
   TProfile *h_allPFO_RecoEff_Mu_pt40to60_hist;
   TProfile *h_allPFO_RecoEff_Mu_pt60to120_hist;
   TProfile *h_allPFO_RecoEff_Mu_pt120to1000_hist;
   TProfile *h_allPFO_RecoEff_NPV_hist;
   TProfile *h_allPFO_RecoEff_NPV_pt10to15_hist;
   TProfile *h_allPFO_RecoEff_NPV_pt15to20_hist;
   TProfile *h_allPFO_RecoEff_NPV_pt20to25_hist;
   TProfile *h_allPFO_RecoEff_NPV_pt25to30_hist;
   TProfile *h_allPFO_RecoEff_NPV_pt30to40_hist;
   TProfile *h_allPFO_RecoEff_NPV_pt40to60_hist;
   TProfile *h_allPFO_RecoEff_NPV_pt60to120_hist;
   TProfile *h_allPFO_RecoEff_NPV_pt120to1000_hist;
   TProfile *h_allPFOTS_RecoEff_Pt_hist;
   TProfile *h_allPFOTS_RecoEff_Eta_hist;
   TProfile *h_allPFOTS_RecoEff_Mu_hist;
   TProfile *h_allPFOTS_RecoEff_Mu_pt10to15_hist;
   TProfile *h_allPFOTS_RecoEff_Mu_pt15to20_hist;
   TProfile *h_allPFOTS_RecoEff_Mu_pt20to25_hist;
   TProfile *h_allPFOTS_RecoEff_Mu_pt25to30_hist;
   TProfile *h_allPFOTS_RecoEff_Mu_pt30to40_hist;
   TProfile *h_allPFOTS_RecoEff_Mu_pt40to60_hist;
   TProfile *h_allPFOTS_RecoEff_Mu_pt60to120_hist;
   TProfile *h_allPFOTS_RecoEff_Mu_pt120to1000_hist;
   TProfile *h_allPFOTS_RecoEff_NPV_hist;
   TProfile *h_allPFOTS_RecoEff_NPV_pt10to15_hist;
   TProfile *h_allPFOTS_RecoEff_NPV_pt15to20_hist;
   TProfile *h_allPFOTS_RecoEff_NPV_pt20to25_hist;
   TProfile *h_allPFOTS_RecoEff_NPV_pt25to30_hist;
   TProfile *h_allPFOTS_RecoEff_NPV_pt30to40_hist;
   TProfile *h_allPFOTS_RecoEff_NPV_pt40to60_hist;
   TProfile *h_allPFOTS_RecoEff_NPV_pt60to120_hist;
   TProfile *h_allPFOTS_RecoEff_NPV_pt120to1000_hist;
   TProfile *h_stdjet_RecoEff_Pt_hist;
   TProfile *h_stdjet_RecoEff_Eta_hist;
   TProfile *h_stdjet_RecoEff_Mu_hist;
   TProfile *h_stdjet_RecoEff_Mu_pt10to15_hist;
   TProfile *h_stdjet_RecoEff_Mu_pt15to20_hist;
   TProfile *h_stdjet_RecoEff_Mu_pt20to25_hist;
   TProfile *h_stdjet_RecoEff_Mu_pt25to30_hist;
   TProfile *h_stdjet_RecoEff_Mu_pt30to40_hist;
   TProfile *h_stdjet_RecoEff_Mu_pt40to60_hist;
   TProfile *h_stdjet_RecoEff_Mu_pt60to120_hist;
   TProfile *h_stdjet_RecoEff_Mu_pt120to1000_hist;
   TProfile *h_stdjet_RecoEff_NPV_hist;
   TProfile *h_stdjet_RecoEff_NPV_pt10to15_hist;
   TProfile *h_stdjet_RecoEff_NPV_pt15to20_hist;
   TProfile *h_stdjet_RecoEff_NPV_pt20to25_hist;
   TProfile *h_stdjet_RecoEff_NPV_pt25to30_hist;
   TProfile *h_stdjet_RecoEff_NPV_pt30to40_hist;
   TProfile *h_stdjet_RecoEff_NPV_pt40to60_hist;
   TProfile *h_stdjet_RecoEff_NPV_pt60to120_hist;
   TProfile *h_stdjet_RecoEff_NPV_pt120to1000_hist;

   TProfile *h_allPFO_RecoEff_Pt_NPV45to50_hist;
   TH1D *h_allPFO_FakeRate_Pt_NPV45to50_hist;
   TProfile *h_allPFO_RecoEff_Eta_NPV45to50_hist;
   TH1D *h_allPFO_FakeRate_Eta_NPV45to50_hist;
   TProfile *h_allPFOTS_RecoEff_Pt_NPV45to50_hist;
   TH1D *h_allPFOTS_FakeRate_Pt_NPV45to50_hist;
   TProfile *h_allPFOTS_RecoEff_Eta_NPV45to50_hist;
   TH1D *h_allPFOTS_FakeRate_Eta_NPV45to50_hist;
   TProfile *h_stdjet_RecoEff_Pt_NPV45to50_hist;
   TH1D *h_stdjet_FakeRate_Pt_NPV45to50_hist;
   TProfile *h_stdjet_RecoEff_Eta_NPV45to50_hist;
   TH1D *h_stdjet_FakeRate_Eta_NPV45to50_hist;

   TH1D *h_allPFO_MatchedTruthJetsNum_Pt_NPV45to50_hist;
   TH1D *h_allPFO_TruthJetsNum_Pt_NPV45to50_hist; 
   TH1D *h_allPFOTS_MatchedTruthJetsNum_Pt_NPV45to50_hist;
   TH1D *h_allPFOTS_TruthJetsNum_Pt_NPV45to50_hist; 
   TH1D *h_stdjet_MatchedTruthJetsNum_Pt_NPV45to50_hist;
   TH1D *h_stdjet_TruthJetsNum_Pt_NPV45to50_hist; 

/*   TH1D *h_allPFO_MatchToLeadingTruthJet_charged_Pt_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_neutral_Pt_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_Eta_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_Eta_pt10to15_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_Eta_pt15to20_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_Eta_pt20to25_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_Eta_pt25to30_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_Eta_pt30to40_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_Eta_pt40to60_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_Eta_pt60to120_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_Eta_pt120to1000_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_ConstitNum_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_ConstitNum_pt10to15_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_ConstitNum_pt15to20_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_ConstitNum_pt20to25_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_ConstitNum_pt25to30_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_ConstitNum_pt30to40_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_ConstitNum_pt40to60_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_ConstitNum_pt60to120_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_ConstitNum_pt120to1000_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_charged_ConstitNum_hist;
   TH1D *h_allPFO_MatchToLeadingTruthJet_neutral_ConstitNum_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_Pt_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_charged_Pt_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_neutral_Pt_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_Eta_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_Eta_pt10to15_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_Eta_pt15to20_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_Eta_pt20to25_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_Eta_pt25to30_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_Eta_pt30to40_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_Eta_pt40to60_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_Eta_pt60to120_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_Eta_pt120to1000_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_ConstitNum_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_ConstitNum_pt10to15_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_ConstitNum_pt15to20_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_ConstitNum_pt20to25_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_ConstitNum_pt25to30_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_ConstitNum_pt30to40_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_ConstitNum_pt40to60_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_ConstitNum_pt60to120_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_ConstitNum_pt120to1000_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_charged_ConstitNum_hist;
   TH1D *h_allPFOTS_MatchToLeadingTruthJet_neutral_ConstitNum_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_Pt_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_Eta_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_Eta_pt10to15_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_Eta_pt15to20_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_Eta_pt20to25_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_Eta_pt25to30_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_Eta_pt30to40_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_Eta_pt40to60_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_Eta_pt60to120_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_Eta_pt120to1000_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_ConstitNum_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_ConstitNum_pt10to15_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_ConstitNum_pt15to20_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_ConstitNum_pt20to25_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_ConstitNum_pt25to30_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_ConstitNum_pt30to40_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_ConstitNum_pt40to60_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_ConstitNum_pt60to120_hist;
   TH1D *h_stdjet_MatchToLeadingTruthJet_ConstitNum_pt120to1000_hist;

   TH1D *h_truthleadingjet_pt_hist;
   TH1D *h_truthleadingjet_eta_hist;
  
   //TProfile *h_allPFOTS_NeutralParticlesOnly_Pt_hist;
   TH1D *h_stdjet_MatchToPFOJet_Pt_hist;
   TH1D *h_stdjet_MatchToPFOJet_Eta_hist;
   TH1D *h_allPFO_MatchToStdJet_DeltaR_hist;
   TH1D *h_allPFOTS_MatchToStdJet_DeltaR_hist;
   TH1D *h_allPFO_MatchToStdJet_Pt_hist;
   TH1D *h_allPFOTS_MatchToStdJet_Pt_hist;
   TH1D *h_allPFO_MatchToStdJet_Eta_hist;
   TH1D *h_allPFOTS_MatchToStdJet_Eta_hist;
   TH2D *h_allPFO_MatchToStdJet_2D_Pt_hist;
   TH2D *h_allPFOTS_MatchToStdJet_2D_Pt_hist;
   TH2D *h_allPFO_MatchToStdJet_2D_Eta_hist;
   TH2D *h_allPFOTS_MatchToStdJet_2D_Eta_hist;
   //TProfile2D *h_allPFO_fakeJetNPV_pt20_hist;
   //TProfile2D *h_allPFO_JetEffNPV_pt20_hist;
*/  
 
   // Declaration of leaf types
   Int_t           el_n;
   Int_t           mu_n;
   std::vector<double>  *mu_tlv_pt;
   std::vector<double>  *mu_tlv_eta;
   std::vector<double>  *mu_tlv_phi;
   std::vector<double>  *mu_tlv_m;
   std::vector<bool>    *mu_signal;
   std::vector<char>    *mu_passOR;
   std::vector<float>   *mu_charge;
   Int_t           jet_n;
   std::vector<double>  *jet_tlv_pt;
   std::vector<double>  *jet_tlv_eta;
   std::vector<double>  *jet_tlv_phi;
   std::vector<double>  *jet_tlv_m;
   std::vector<int>     *jet_truthJetPt;
   std::vector<float>   *jet_jvt;
   std::vector<float>   *jet_Jvt;
   std::vector<char>    *jet_baseline;
   std::vector<int>     *jet_isPileUpJet;
   std::vector<char>    *jet_passOR;
   std::vector<float>   *jet_JvtJvfcorr;
   std::vector<float>   *jet_DetectorEta;
   std::vector<double>  *jet_JvtRpt;
   std::vector<float>   *jet_SumTrkPt500;
   std::vector<float>   *jet_SumTrkPt1000;
   std::vector<float>   *jet_ConstitScaleMomentum_pt;
   std::vector<float>   *jet_ConstitScaleMomentum_eta;
   std::vector<float>   *jet_ConstitScaleMomentum_phi;
   std::vector<int>     *jet_isTruthJet;
   std::vector<int>     *jet_numConstituents;
   Int_t           trjet_n;
   std::vector<double>  *trjet_tlv_pt;
   std::vector<double>  *trjet_tlv_eta;
   std::vector<double>  *trjet_tlv_phi;
   std::vector<double>  *trjet_tlv_m;
/*   Int_t           chargedPFOjet_n;
   std::vector<double>  *chargedPFOjet_tlv_pt;
   std::vector<double>  *chargedPFOjet_tlv_eta;
   std::vector<double>  *chargedPFOjet_tlv_phi;
   std::vector<double>  *chargedPFOjet_tlv_m;
   std::vector<int>     *chargedPFOjet_isPUJet;
*/
   Int_t           allPFOTSjet_n;
   std::vector<double>  *allPFOTSjet_tlv_pt;
   std::vector<double>  *allPFOTSjet_tlv_eta;
   std::vector<double>  *allPFOTSjet_tlv_phi;
   std::vector<double>  *allPFOTSjet_tlv_m;
   std::vector<int>     *allPFOTSjet_isPUJet;
   std::vector<int>     *allPFOTSjet_withChargedParticle;
   std::vector<double>     *allPFOTSjet_ChargedConstitPt;
   std::vector<int>     *allPFOTSjet_ConstituentsNumber;
   Int_t           allPFOjet_n;
   std::vector<double>  *allPFOjet_tlv_pt;
   std::vector<double>  *allPFOjet_tlv_eta;
   std::vector<double>  *allPFOjet_tlv_phi;
   std::vector<double>  *allPFOjet_tlv_m;
   std::vector<int>     *allPFOjet_isPUJet;
   std::vector<int>     *allPFOjet_withChargedParticle;
   std::vector<double>     *allPFOjet_ChargedConstitPt;
   std::vector<int>     *allPFOjet_ConstituentsNumber;
   
   Bool_t          HLT_mu60_0eta105_msonly;
   Bool_t          HLT_mu40;
   Bool_t          HLT_mu24_imedium;
   Bool_t          HLT_mu24_ivarloose;
   Bool_t          HLT_mu24_iloose;
   Bool_t          HLT_e300_etcut;
   Bool_t          HLT_e26_lhtight_nod0;
   Bool_t          HLT_e24_lhtight_nod0_ivarloose;
   Float_t         pvIz;
   Float_t         vtx_z;
   UInt_t          ChannelNumber;
   Bool_t          HLT_e140_lhloose_nod0;
   Bool_t          HLT_e60_medium;
   Float_t         EventWeight;
   UInt_t          lbn;
   Float_t         totalEvents;
   Float_t         PileupWeight;
   Float_t         totalWeightedEvents;
   UInt_t          bcid;
   ULong64_t       vtx_nTracks;
   Bool_t          HLT_mu26_ivarmedium;
   Float_t         actualInteractionsPerCrossing;
   Bool_t          HLT_e26_lhtight_nod0_ivarloose;
   Int_t           hasPV;
   Double_t        MET_RefFinal_TST_ety;
   Bool_t          HLT_e60_lhmedium_nod0;
   Double_t        MET_RefFinal_TST_et;
   Float_t         averageInteractionsPerCrossing;
   Double_t        MET_RefFinal_TST_etx;
   Double_t        MET_RefFinal_TST_SumET;
   Double_t        MET_RefJet_TST_et;
   Double_t        MET_RefJet_TST_etx;
   Bool_t          HLT_mu50;
   Double_t        MET_RefJet_TST_ety;
   Bool_t          isSimulation;
   UInt_t          RunNumber;
   Int_t           passBadJet;
   Int_t           NPV_2trk;
   Int_t           NPV_4trk;
   Double_t        MET_RefJet_TST_SumET;
   std::vector<double>  *el_tlv_pt;
   std::vector<double>  *el_tlv_eta;
   std::vector<double>  *el_tlv_phi;
   std::vector<double>  *el_tlv_m;
   std::vector<bool>    *el_signal;
   std::vector<char>    *el_passOR;
   std::vector<float>   *el_charge;

   // List of branches
   TBranch        *b_el_n;   //!
   TBranch        *b_mu_n;   //!
   TBranch        *b_mu_tlv_pt;   //!
   TBranch        *b_mu_tlv_eta;   //!
   TBranch        *b_mu_tlv_phi;   //!
   TBranch        *b_mu_tlv_m;   //!
   TBranch        *b_mu_signal;   //!
   TBranch        *b_mu_passOR;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_jet_n;   //!
   TBranch        *b_jet_tlv_pt;   //!
   TBranch        *b_jet_tlv_eta;   //!
   TBranch        *b_jet_tlv_phi;   //!
   TBranch        *b_jet_tlv_m;   //!
   TBranch        *b_jet_truthJetPt;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_Jvt;   //!
   TBranch        *b_jet_baseline;   //!
   TBranch        *b_jet_isPileUpJet;   //!
   TBranch        *b_jet_passOR;   //!
   TBranch        *b_jet_JvtJvfcorr;   //!
   TBranch        *b_jet_DetectorEta;   //!
   TBranch        *b_jet_JvtRpt;   //!
   TBranch        *b_jet_SumTrkPt500;   //!
   TBranch        *b_jet_SumTrkPt1000;   //!
   TBranch        *b_jet_ConstitScaleMomentum_pt;   //!
   TBranch        *b_jet_ConstitScaleMomentum_eta;   //!
   TBranch        *b_jet_ConstitScaleMomentum_phi;   //!
   TBranch        *b_jet_isTruthJet;   //!
   TBranch        *b_jet_numConstituents;
   TBranch        *b_trjet_n;   //!
   TBranch        *b_trjet_tlv_pt;   //!
   TBranch        *b_trjet_tlv_eta;   //!
   TBranch        *b_trjet_tlv_phi;   //!
   TBranch        *b_trjet_tlv_m;   //!
/*   TBranch        *b_chargedPFOjet_n;
   TBranch        *b_chargedPFOjet_tlv_pt;
   TBranch        *b_chargedPFOjet_tlv_eta;
   TBranch        *b_chargedPFOjet_tlv_phi;
   TBranch        *b_chargedPFOjet_tlv_m;
   TBranch        *b_chargedPFOjet_isPUJet;
*/
   TBranch        *b_allPFOjet_n;
   TBranch        *b_allPFOjet_ConstituentsNumber;
   TBranch        *b_allPFOjet_withChargedParticle;
   TBranch        *b_allPFOjet_ChargedConstitPt;
   TBranch        *b_allPFOjet_tlv_pt;
   TBranch        *b_allPFOjet_tlv_eta;
   TBranch        *b_allPFOjet_tlv_phi;
   TBranch        *b_allPFOjet_tlv_m;
   TBranch        *b_allPFOjet_isPUJet;
   TBranch        *b_allPFOTSjet_n;
   TBranch        *b_allPFOTSjet_ConstituentsNumber;
   TBranch        *b_allPFOTSjet_withChargedParticle;
   TBranch        *b_allPFOTSjet_ChargedConstitPt;
   TBranch        *b_allPFOTSjet_tlv_pt;
   TBranch        *b_allPFOTSjet_tlv_eta;
   TBranch        *b_allPFOTSjet_tlv_phi;
   TBranch        *b_allPFOTSjet_tlv_m;
   TBranch        *b_allPFOTSjet_isPUJet;
   TBranch        *b_HLT_mu60_0eta105_msonly;   //!
   TBranch        *b_HLT_mu40;   //!
   TBranch        *b_HLT_mu24_imedium;   //!
   TBranch        *b_HLT_mu24_ivarloose;   //!
   TBranch        *b_HLT_mu24_iloose;   //!
   TBranch        *b_HLT_e300_etcut;   //!
   TBranch        *b_HLT_e26_lhtight_nod0;   //!
   TBranch        *b_HLT_e24_lhtight_nod0_ivarloose;   //!
   TBranch        *b_pvIz;   //!
   TBranch        *b_vtx_z;   //!
   TBranch        *b_ChannelNumber;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_e60_medium;   //!
   TBranch        *b_EventWeight;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_totalEvents;   //!
   TBranch        *b_PileupWeight;   //!
   TBranch        *b_totalWeightedEvents;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_vtx_nTracks;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_actualInteractionsPerCrossing;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_hasPV;   //!
   TBranch        *b_MET_RefFinal_TST_ety;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_MET_RefFinal_TST_et;   //!
   TBranch        *b_averageInteractionsPerCrossing;   //!
   TBranch        *b_MET_RefFinal_TST_etx;   //!
   TBranch        *b_MET_RefFinal_TST_SumET;   //!
   TBranch        *b_MET_RefJet_TST_et;   //!
   TBranch        *b_MET_RefJet_TST_etx;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_MET_RefJet_TST_ety;   //!
   TBranch        *b_isSimulation;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_passBadJet;   //!
   TBranch        *b_NPV_2trk;
   TBranch        *b_NPV_4trk;
   TBranch        *b_MET_RefJet_TST_SumET;   //!
   TBranch        *b_el_tlv_pt;   //!
   TBranch        *b_el_tlv_eta;   //!
   TBranch        *b_el_tlv_phi;   //!
   TBranch        *b_el_tlv_m;   //!
   TBranch        *b_el_signal;   //!
   TBranch        *b_el_passOR;   //!
   TBranch        *b_el_charge;   //!


   MyEvent(TTree * /*tree*/ =0) { }
   virtual ~MyEvent() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(MyEvent,0);

 private:

//Put Private things here eg. SUSYTools, GRL

};

#endif

#ifdef MyEvent_cxx
void MyEvent::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mu_tlv_pt = 0;
   mu_tlv_eta = 0;
   mu_tlv_phi = 0;
   mu_tlv_m = 0;
   mu_signal = 0;
   mu_passOR = 0;
   mu_charge = 0;
   jet_tlv_pt = 0;
   jet_tlv_eta = 0;
   jet_tlv_phi = 0;
   jet_tlv_m = 0;
   jet_truthJetPt = 0;
   jet_jvt = 0;
   jet_Jvt = 0;
   jet_baseline = 0;
   jet_isPileUpJet = 0;
   jet_passOR = 0;
   jet_JvtJvfcorr = 0;
   jet_DetectorEta = 0;
   jet_JvtRpt = 0;
   jet_SumTrkPt500 = 0;
   jet_ConstitScaleMomentum_pt = 0;
   jet_ConstitScaleMomentum_eta = 0;
   jet_ConstitScaleMomentum_phi = 0;
   jet_SumTrkPt1000 = 0;
   jet_isTruthJet = 0;
   jet_numConstituents = 0;
   trjet_tlv_pt = 0;
   trjet_tlv_eta = 0;
   trjet_tlv_phi = 0;
   trjet_tlv_m = 0;
/*   chargedPFOjet_tlv_pt = 0;
   chargedPFOjet_tlv_eta = 0;
   chargedPFOjet_tlv_phi = 0;
   chargedPFOjet_tlv_m = 0;
   chargedPFOjet_isPUJet = 0;*/
   allPFOjet_ConstituentsNumber = 0;
   allPFOjet_withChargedParticle = 0;
   allPFOjet_ChargedConstitPt = 0;
   allPFOjet_tlv_pt = 0;
   allPFOjet_tlv_eta = 0;
   allPFOjet_tlv_phi = 0;
   allPFOjet_tlv_m = 0;
   allPFOjet_isPUJet = 0;
   allPFOTSjet_ConstituentsNumber = 0;
   allPFOTSjet_withChargedParticle = 0;
   allPFOTSjet_ChargedConstitPt = 0;
   allPFOTSjet_tlv_pt = 0;
   allPFOTSjet_tlv_eta = 0;
   allPFOTSjet_tlv_phi = 0;
   allPFOTSjet_tlv_m = 0;
   allPFOTSjet_isPUJet = 0;
   el_tlv_pt = 0;
   el_tlv_eta = 0;
   el_tlv_phi = 0;
   el_tlv_m = 0;
   el_signal = 0;
   el_passOR = 0;
   el_charge = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("el_n", &el_n, &b_el_n);
   fChain->SetBranchAddress("mu_n", &mu_n, &b_mu_n);
   fChain->SetBranchAddress("mu_tlv_pt", &mu_tlv_pt, &b_mu_tlv_pt);
   fChain->SetBranchAddress("mu_tlv_eta", &mu_tlv_eta, &b_mu_tlv_eta);
   fChain->SetBranchAddress("mu_tlv_phi", &mu_tlv_phi, &b_mu_tlv_phi);
   fChain->SetBranchAddress("mu_tlv_m", &mu_tlv_m, &b_mu_tlv_m);
   fChain->SetBranchAddress("mu_signal", &mu_signal, &b_mu_signal);
   fChain->SetBranchAddress("mu_passOR", &mu_passOR, &b_mu_passOR);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("jet_n", &jet_n, &b_jet_n);
   fChain->SetBranchAddress("jet_tlv_pt", &jet_tlv_pt, &b_jet_tlv_pt);
   fChain->SetBranchAddress("jet_tlv_eta", &jet_tlv_eta, &b_jet_tlv_eta);
   fChain->SetBranchAddress("jet_tlv_phi", &jet_tlv_phi, &b_jet_tlv_phi);
   fChain->SetBranchAddress("jet_tlv_m", &jet_tlv_m, &b_jet_tlv_m);
   fChain->SetBranchAddress("jet_truthJetPt", &jet_truthJetPt, &b_jet_truthJetPt);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("jet_Jvt", &jet_Jvt, &b_jet_Jvt);
   fChain->SetBranchAddress("jet_baseline", &jet_baseline, &b_jet_baseline);
   fChain->SetBranchAddress("jet_isPileUpJet", &jet_isPileUpJet, &b_jet_isPileUpJet);
   fChain->SetBranchAddress("jet_passOR", &jet_passOR, &b_jet_passOR);
   fChain->SetBranchAddress("jet_JvtJvfcorr", &jet_JvtJvfcorr, &b_jet_JvtJvfcorr);
   fChain->SetBranchAddress("jet_DetectorEta", &jet_DetectorEta, &b_jet_DetectorEta);
   fChain->SetBranchAddress("jet_JvtRpt", &jet_JvtRpt, &b_jet_JvtRpt);
   fChain->SetBranchAddress("jet_SumTrkPt500", &jet_SumTrkPt500, &b_jet_SumTrkPt500);
   fChain->SetBranchAddress("jet_SumTrkPt1000", &jet_SumTrkPt1000, &b_jet_SumTrkPt1000);
   fChain->SetBranchAddress("jet_ConstitScaleMomentum_pt", &jet_ConstitScaleMomentum_pt, &b_jet_ConstitScaleMomentum_pt);
   fChain->SetBranchAddress("jet_ConstitScaleMomentum_eta", &jet_ConstitScaleMomentum_eta, &b_jet_ConstitScaleMomentum_eta);
   fChain->SetBranchAddress("jet_ConstitScaleMomentum_phi", &jet_ConstitScaleMomentum_phi, &b_jet_ConstitScaleMomentum_phi);
   fChain->SetBranchAddress("jet_isTruthJet", &jet_isTruthJet, &b_jet_isTruthJet);
   fChain->SetBranchAddress("jet_numConstituents", &jet_numConstituents, &b_jet_numConstituents);
   fChain->SetBranchAddress("trjet_n", &trjet_n, &b_trjet_n);
   fChain->SetBranchAddress("trjet_tlv_pt", &trjet_tlv_pt, &b_trjet_tlv_pt);
   fChain->SetBranchAddress("trjet_tlv_eta", &trjet_tlv_eta, &b_trjet_tlv_eta);
   fChain->SetBranchAddress("trjet_tlv_phi", &trjet_tlv_phi, &b_trjet_tlv_phi);
   fChain->SetBranchAddress("trjet_tlv_m", &trjet_tlv_m, &b_trjet_tlv_m);
/*   fChain->SetBranchAddress("chargedPFOjet_n", &chargedPFOjet_n, &b_chargedPFOjet_n);
   fChain->SetBranchAddress("chargedPFOjet_tlv_pt", &chargedPFOjet_tlv_pt, &b_chargedPFOjet_tlv_pt);
   fChain->SetBranchAddress("chargedPFOjet_tlv_eta", &chargedPFOjet_tlv_eta, &b_chargedPFOjet_tlv_eta);
   fChain->SetBranchAddress("chargedPFOjet_tlv_phi", &chargedPFOjet_tlv_phi, &b_chargedPFOjet_tlv_phi);
   fChain->SetBranchAddress("chargedPFOjet_tlv_m", &chargedPFOjet_tlv_m, &b_chargedPFOjet_tlv_m);

   fChain->SetBranchAddress("chargedPFOjet_isPUJet", &chargedPFOjet_isPUJet, &b_chargedPFOjet_isPUJet);
*/
   fChain->SetBranchAddress("allPFOjet_withChargedParticle", &allPFOjet_withChargedParticle, &b_allPFOjet_withChargedParticle);
   fChain->SetBranchAddress("allPFOjet_ChargedConstitPt", &allPFOjet_ChargedConstitPt, &b_allPFOjet_ChargedConstitPt);
   fChain->SetBranchAddress("allPFOjet_ConstituentsNumber", &allPFOjet_ConstituentsNumber, &b_allPFOjet_ConstituentsNumber);
   fChain->SetBranchAddress("allPFOjet_n", &allPFOjet_n, &b_allPFOjet_n);
   fChain->SetBranchAddress("allPFOjet_tlv_pt", &allPFOjet_tlv_pt, &b_allPFOjet_tlv_pt);
   fChain->SetBranchAddress("allPFOjet_tlv_eta", &allPFOjet_tlv_eta, &b_allPFOjet_tlv_eta);
   fChain->SetBranchAddress("allPFOjet_tlv_phi", &allPFOjet_tlv_phi, &b_allPFOjet_tlv_phi);
   fChain->SetBranchAddress("allPFOjet_tlv_m", &allPFOjet_tlv_m, &b_allPFOjet_tlv_m);
   fChain->SetBranchAddress("allPFOjet_isPUJet", &allPFOjet_isPUJet, &b_allPFOjet_isPUJet);
   fChain->SetBranchAddress("allPFOTSjet_withChargedParticle", &allPFOTSjet_withChargedParticle, &b_allPFOTSjet_withChargedParticle);
   fChain->SetBranchAddress("allPFOTSjet_ChargedConstitPt", &allPFOTSjet_ChargedConstitPt, &b_allPFOTSjet_ChargedConstitPt);
   fChain->SetBranchAddress("allPFOTSjet_ConstituentsNumber", &allPFOTSjet_ConstituentsNumber, &b_allPFOTSjet_ConstituentsNumber);
   fChain->SetBranchAddress("allPFOTSjet_n", &allPFOTSjet_n, &b_allPFOTSjet_n);
   fChain->SetBranchAddress("allPFOTSjet_tlv_pt", &allPFOTSjet_tlv_pt, &b_allPFOTSjet_tlv_pt);
   fChain->SetBranchAddress("allPFOTSjet_tlv_eta", &allPFOTSjet_tlv_eta, &b_allPFOTSjet_tlv_eta);
   fChain->SetBranchAddress("allPFOTSjet_tlv_phi", &allPFOTSjet_tlv_phi, &b_allPFOTSjet_tlv_phi);
   fChain->SetBranchAddress("allPFOTSjet_tlv_m", &allPFOTSjet_tlv_m, &b_allPFOTSjet_tlv_m);
   fChain->SetBranchAddress("allPFOTSjet_isPUJet", &allPFOTSjet_isPUJet, &b_allPFOTSjet_isPUJet);
   fChain->SetBranchAddress("HLT_mu60_0eta105_msonly", &HLT_mu60_0eta105_msonly, &b_HLT_mu60_0eta105_msonly);
   fChain->SetBranchAddress("HLT_mu40", &HLT_mu40, &b_HLT_mu40);
   fChain->SetBranchAddress("HLT_mu24_imedium", &HLT_mu24_imedium, &b_HLT_mu24_imedium);
   fChain->SetBranchAddress("HLT_mu24_ivarloose", &HLT_mu24_ivarloose, &b_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("HLT_mu24_iloose", &HLT_mu24_iloose, &b_HLT_mu24_iloose);
   fChain->SetBranchAddress("HLT_e300_etcut", &HLT_e300_etcut, &b_HLT_e300_etcut);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0", &HLT_e26_lhtight_nod0, &b_HLT_e26_lhtight_nod0);
   fChain->SetBranchAddress("HLT_e24_lhtight_nod0_ivarloose", &HLT_e24_lhtight_nod0_ivarloose, &b_HLT_e24_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("pvIz", &pvIz, &b_pvIz);
   fChain->SetBranchAddress("vtx_z", &vtx_z, &b_vtx_z);
   fChain->SetBranchAddress("ChannelNumber", &ChannelNumber, &b_ChannelNumber);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("HLT_e60_medium", &HLT_e60_medium, &b_HLT_e60_medium);
   fChain->SetBranchAddress("EventWeight", &EventWeight, &b_EventWeight);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("totalEvents", &totalEvents, &b_totalEvents);
   fChain->SetBranchAddress("PileupWeight", &PileupWeight, &b_PileupWeight);
   fChain->SetBranchAddress("totalWeightedEvents", &totalWeightedEvents, &b_totalWeightedEvents);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("vtx_nTracks", &vtx_nTracks, &b_vtx_nTracks);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing, &b_actualInteractionsPerCrossing);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("hasPV", &hasPV, &b_hasPV);
   fChain->SetBranchAddress("MET_RefFinal_TST_ety", &MET_RefFinal_TST_ety, &b_MET_RefFinal_TST_ety);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("MET_RefFinal_TST_et", &MET_RefFinal_TST_et, &b_MET_RefFinal_TST_et);
   fChain->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing, &b_averageInteractionsPerCrossing);
   fChain->SetBranchAddress("MET_RefFinal_TST_etx", &MET_RefFinal_TST_etx, &b_MET_RefFinal_TST_etx);
   fChain->SetBranchAddress("MET_RefFinal_TST_SumET", &MET_RefFinal_TST_SumET, &b_MET_RefFinal_TST_SumET);
   fChain->SetBranchAddress("MET_RefJet_TST_et", &MET_RefJet_TST_et, &b_MET_RefJet_TST_et);
   fChain->SetBranchAddress("MET_RefJet_TST_etx", &MET_RefJet_TST_etx, &b_MET_RefJet_TST_etx);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("MET_RefJet_TST_ety", &MET_RefJet_TST_ety, &b_MET_RefJet_TST_ety);
   fChain->SetBranchAddress("isSimulation", &isSimulation, &b_isSimulation);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("passBadJet", &passBadJet, &b_passBadJet);
   fChain->SetBranchAddress("NPV_2trk", &NPV_2trk, &b_NPV_2trk);
   fChain->SetBranchAddress("NPV_4trk", &NPV_4trk, &b_NPV_4trk);
   fChain->SetBranchAddress("MET_RefJet_TST_SumET", &MET_RefJet_TST_SumET, &b_MET_RefJet_TST_SumET);
   fChain->SetBranchAddress("el_tlv_pt", &el_tlv_pt, &b_el_tlv_pt);
   fChain->SetBranchAddress("el_tlv_eta", &el_tlv_eta, &b_el_tlv_eta);
   fChain->SetBranchAddress("el_tlv_phi", &el_tlv_phi, &b_el_tlv_phi);
   fChain->SetBranchAddress("el_tlv_m", &el_tlv_m, &b_el_tlv_m);
   fChain->SetBranchAddress("el_signal", &el_signal, &b_el_signal);
   fChain->SetBranchAddress("el_passOR", &el_passOR, &b_el_passOR);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
}

Bool_t MyEvent::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef MyEvent_cxx
